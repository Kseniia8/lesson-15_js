document.addEventListener('DOMContentLoaded', function() {
    const buttons = document.querySelectorAll('.key-button');
    let activeButton = null;
  
    document.addEventListener('keydown', function(event) {
      let key = event.key;
      
      // Handle special key names like "Enter"
      if (key === 'Enter') {
        key = 'Enter';
      }
  
      // Find the button that matches the pressed key
      const button = Array.from(buttons).find(btn => btn.textContent.trim().toLowerCase() === key.toLowerCase());
  
      if (button) {
        // Remove highlight from previously active button
        if (activeButton) {
          activeButton.classList.remove('highlighted');
        }
  
        // Highlight the new button
        button.classList.add('highlighted');
        activeButton = button;
      } else {
        // If the key doesn't match any button, remove highlight from the active button
        if (activeButton) {
          activeButton.classList.remove('highlighted');
          activeButton = null;
        }
      }
    });
  });
  